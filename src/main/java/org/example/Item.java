package org.example;


import java.util.HashMap;

public class Item {
    private String itemName;
    private String itemCategory;
    private int itemQuantity;
    private double itemPrice;
    private final int itemId;

    public Item(String name, String category, double price, int quantity, int id) {
        this.itemName = formatItemName(name);
        this.itemQuantity = quantity;
        this.itemPrice = price;
        this.itemCategory = category;
        this.itemId = id;
    }


    @Override
    public String toString() {
        HashMap<Object, Object> map = new HashMap<>();
        map.put("Category", this.itemCategory);
        map.put("Name", this.itemName);
        map.put("Quantity", this.itemQuantity);
        map.put("Price", this.itemPrice);
        map.put("ID", this.itemId);
        return map.toString();
    }

    protected String getItemName() {
        return this.itemName;
    }

    protected void setItemName(String name) {
        this.itemName = name;
    }

    protected int getItemQuantity() {
        return this.itemQuantity;
    }

    protected void setItemQuantity(int quantity) {
        this.itemQuantity = quantity;
    }

    protected double getItemPrice() {
        return this.itemPrice;
    }

    protected void setItemPrice(double price) {
        this.itemPrice = price;
    }

    protected String getItemCategory() {
        return this.itemCategory;
    }

    protected void setItemCategory(String category) {
        this.itemCategory = category;
    }

    protected int getItemId(){
        return this.itemId;
    }

    protected static String formatItemName(String name){
        name = name.toLowerCase().strip();

        String firstLetter = name.substring(0,1).toUpperCase();
        String restOfWord = name.substring(1);
        return firstLetter + restOfWord;
    }
}
