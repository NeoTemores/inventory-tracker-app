package org.example;

public class Beverage extends Item{
    public Beverage (String name, String category, double price, int quantity, int id){
        super(name, category, price, quantity, id);
    }
}
