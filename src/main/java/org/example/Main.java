package org.example;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        System.out.println("\nWelcome to Neo's Inventory App!");
        System.out.println("---------------------------------");

        try {
            startProgram();
        } catch (IOException e) {
            System.out.println("\nApplication Error: " + e.getMessage());
        }
    }

    static void startProgram() throws IOException {

        boolean applicationIsRunning = true;
        Inventory inventory = new Inventory();
        inventory.loadInventory(inventory.getInventoryList());

        Scanner inputScanner = new Scanner(System.in);

        while (applicationIsRunning) {

            printMenu();
            String selection = inputScanner.nextLine();

            switch (selection) {
                case "1":
                    inputNewItem(inputScanner, inventory);
                    break;
                case "2":
                    deleteItemFromInventory(inputScanner, inventory);
                    break;

                case "3":
                    updateInventory(inputScanner, inventory);
                    break;
                case "4":
                    displayCurrentInventory(inventory);
                    break;
                case "0":
                    inventory.saveInventory(inventory.getInventoryList());
                    applicationIsRunning = false;
                    System.out.println("\n> Inventory successfully saved. Goodbye.");
                    break;
                default:
                    System.out.println("> \"" + selection + "\" is invalid.");
            }
        }
    }

    static void inputNewItem(Scanner inputScanner, Inventory inventory) {
        System.out.println("\n> Add Item to Inventory:");
        boolean addingItems = true;
        while (addingItems) {

            String category = processSelectCategory(inputScanner);

            String itemName = processValidName(inputScanner);

            double price = processValidPrice(inputScanner);

            int quantity = processValidQty(inputScanner);

            Random random = new Random();
            int newItemId = random.nextInt(9999) + inventory.getInventoryList().size();

            Item item = processCreateNewItem(itemName, category, price, quantity, newItemId);
            inventory.addToInventory(item);

            System.out.println("> Added new Item: " + item);

            System.out.print("-> Add another item? (y/n): ");
            String keepAdding = inputScanner.nextLine();
            addingItems = keepAdding.equalsIgnoreCase("y") || keepAdding.equalsIgnoreCase("yes");
        }
    }

    static void deleteItemFromInventory(Scanner inputScanner, Inventory inventory) {
        System.out.println("\n> Delete Item From Inventory:");
        if (inventory.getInventoryList().size() == 0) {
            System.out.println("> Nothing found in inventory");
        } else {

            int itemIdToDelete;
            while (true) {
                System.out.print("\n-> Please enter the item id to delete: ");
                String inputId = inputScanner.nextLine();
                if (inputId.length() == 0) break;

                try {
                    itemIdToDelete = Integer.parseInt(inputId);
                    inventory.deleteItemFromInventory(itemIdToDelete, inventory.getInventoryList(), false);
                    break;
                } catch (Exception e) {
                    System.out.println("> \"" + inputId + "\" is an Invalid ID#\n");
                }
            }
        }
    }

    static void updateInventory(Scanner inputScanner, Inventory inventory) {
        System.out.println("\n> Update Inventory");
        int itemIdToUpdate;
        String itemNameToUpdate;

        System.out.print("\n-> Please enter item id or item name: ");
        String updateItemInput = inputScanner.nextLine();
        if (updateItemInput.length() == 0) return;

        try {
            itemIdToUpdate = Integer.parseInt(updateItemInput);
            boolean foundItemID = inventory.searchInventory(inventory.getInventoryList(), itemIdToUpdate);

            if (!foundItemID) System.out.println("> No item found with ID#: " + itemIdToUpdate);
            else processUpdateItem(inventory, inputScanner);

        } catch (Exception e) {
            itemNameToUpdate = Item.formatItemName(updateItemInput);
            boolean foundItemByName = inventory.searchInventory(inventory.getInventoryList(), itemNameToUpdate);

            if (!foundItemByName) System.out.println("> No item found with name: " + itemNameToUpdate);
            else processUpdateItem(inventory, inputScanner);
        }
    }

    static void displayCurrentInventory(Inventory inventory) {
        List<Item> currentInventory = inventory.getInventoryList();
        if (currentInventory.size() == 0) {
            System.out.println("> No inventory found");
        } else {
            System.out.println("\n> View Current Inventory:");
            inventory.printCurrentInventory(currentInventory);
        }
    }

    static void printMenu() {
        System.out.println("\n@ Main Menu");
        System.out.println("----------------");
        System.out.println("1: Add new item to inventory");
        System.out.println("2: Delete item from inventory");
        System.out.println("3: Update inventory");
        System.out.println("4: View current inventory");
        System.out.println("0: Save & Quit");
        System.out.print("\n-> Please enter an option: ");
    }

    static void printUpdateItemMenu() {
        System.out.println("\n@ Update Item Menu");
        System.out.println("----------------");
        System.out.println("1: Update Name");
        System.out.println("2: Update Price");
        System.out.println("3: Update Quantity");
        System.out.println("4: Update Category");
        System.out.println("0: Return to Main Menu");
        System.out.print("\n-> Please enter an option: ");

    }

    static Item processCreateNewItem(String name, String category, double price, int qty, int id) {

        switch (category) {
            case "Food":
                return new Food(name, category, price, qty, id);
            case "Beverage":
                return new Beverage(name, category, price, qty, id);
            case "Household":
                return new Household(name, category, price, qty, id);
            default:
                return new Misc(name, category, price, qty, id);
        }
    }

    static void processUpdateItem(Inventory inventory, Scanner inputScanner) {
        System.out.printf("\n> Current Item to update: %s\n", inventory.getCurrentItemBeingUpdated());
        printUpdateItemMenu();
        String updateSelection = inputScanner.nextLine();
        switch (updateSelection) {
            case "1":
                System.out.println("> Please enter new item Name: ");
                String updatedName = processValidName(inputScanner);
                updatedName = Item.formatItemName(updatedName);
                inventory.updateItemName(inventory.getInventoryList(), inventory.getCurrentItemBeingUpdated(), updatedName);
                break;

            case "2":
                System.out.println("> Please enter new item Price: ");
                double newPrice = processValidPrice(inputScanner);
                inventory.updateItemPrice(inventory.getInventoryList(), inventory.getCurrentItemBeingUpdated(), newPrice);
                break;

            case "3":
                System.out.println("> Please enter new item Quantity: ");
                int newQty = processValidQty(inputScanner);
                inventory.updateItemQty(inventory.getInventoryList(), inventory.getCurrentItemBeingUpdated(), newQty);
                break;

            case "4":
                String newCategory = processSelectCategory(inputScanner);
                Item oldItem = inventory.getCurrentItemBeingUpdated();
                inventory.deleteItemFromInventory(oldItem.getItemId(), inventory.getInventoryList(), true);

                Item newItem = processCreateNewItem(oldItem.getItemName(), newCategory, oldItem.getItemPrice(), oldItem.getItemQuantity(), oldItem.getItemId());
                inventory.addToInventory(newItem);
                System.out.println("> Item CATEGORY successfully updated: " + newItem);

                break;
            case "0":
                break;
            default:
                System.out.printf("> \"%s\" is not valid", updateSelection);
                break;
        }
    }

    static double processValidPrice(Scanner inputScanner) {
        while (true) {
            System.out.print("-> Price (format 00.00): ");
            String inputPrice = inputScanner.nextLine();
            try {
                return Double.parseDouble(inputPrice);
            } catch (Exception e) {
                System.out.printf("> \"%s\" is an invalid price format.\n\n", inputPrice);
            }
        }
    }

    static int processValidQty(Scanner inputScanner) {
        while (true) {
            System.out.print("-> Quantity: ");
            String inputQuantity = inputScanner.nextLine();
            try {
                return Integer.parseInt(inputQuantity);
            } catch (Exception e) {
                System.out.printf("> \"%s\" is an invalid quantity.\n\n", inputQuantity);
            }
        }
    }

    static String processValidName(Scanner inputScanner) {
        while (true) {
            System.out.print("-> Item name: ");
            String itemName = inputScanner.nextLine();
            if (itemName.matches(".*[a-z,A-Z].*")) return itemName;
            System.out.println("> Error: Item name must contain at least 1 letter.");
        }
    }

    static String processSelectCategory(Scanner inputScanner) {
        while (true) {
            System.out.println("\nCategory: ");
            System.out.println("------------");
            System.out.println("1: Food");
            System.out.println("2: Beverage");
            System.out.println("3: Household");
            System.out.println("4: Misc.");
            System.out.print("-> Please select a category: ");
            String category = inputScanner.nextLine();

            switch (category) {
                case "1":
                    return "Food";
                case "2":
                    return "Beverage";
                case "3":
                    return "Household";
                case "4":
                    return "Misc.";
                default:
                    System.out.println("> \"" + category + "\" is invalid.");
            }
        }
    }
}