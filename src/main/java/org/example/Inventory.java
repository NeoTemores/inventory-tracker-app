package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Inventory {
    private final List<Item> INVENTORY_LIST = new ArrayList<>();
    private Item currentItemBeingUpdated;
    private int indexOfItemToUpdate;


    protected List<Item> getInventoryList() {
        return this.INVENTORY_LIST;
    }

    protected void addToInventory(Item item) {
        this.INVENTORY_LIST.add(item);
    }

    protected boolean searchInventory(List<Item> inventoryList, int itemID) {

        for (Item item : inventoryList) {
            if (item.getItemId() == itemID) {

                currentItemBeingUpdated = item;
                indexOfItemToUpdate = inventoryList.indexOf(item);
                return true;
            }
        }
        return false;
    }

    protected boolean searchInventory(List<Item> inventoryList, String itemName) {

        for (Item item : inventoryList) {
            if (item.getItemName().equalsIgnoreCase(itemName)) {

                currentItemBeingUpdated = item;
                indexOfItemToUpdate = inventoryList.indexOf(item);
                return true;
            }
        }
        return false;
    }

    protected Item getCurrentItemBeingUpdated() {
        return this.currentItemBeingUpdated;
    }

    protected void updateItemName(List<Item> inventoryList, Item item, String name) {
        item.setItemName(name);
        inventoryList.set(indexOfItemToUpdate, item);
        System.out.println("> Item NAME successfully updated: " + item);
    }

    protected void updateItemPrice(List<Item> inventoryList, Item item, double newPrice) {
        item.setItemPrice(newPrice);
        inventoryList.set(indexOfItemToUpdate, item);
        System.out.println("> Item PRICE successfully updated: " + item);
    }

    protected void updateItemQty(List<Item> inventoryList, Item item, int newQty) {
        item.setItemQuantity(newQty);
        inventoryList.set(indexOfItemToUpdate, item);
        System.out.println("> Item QTY successfully updated: " + item);
    }

    protected void loadInventory(List<Item> inventory) throws FileNotFoundException {
        Scanner fileReader = new Scanner(new File("src/main/resources/inventory.txt"));

        while (fileReader.hasNext()) {
            String[] parts = fileReader.nextLine().split(",");
            String category = parts[0];
            int id = Integer.parseInt(parts[1]);
            String name = parts[2];
            double price = Double.parseDouble(parts[3]);
            int qty = Integer.parseInt(parts[4]);
            Item item = new Item(name, category, price, qty, id);
            inventory.add(item);
        }
        fileReader.close();
    }

    protected void saveInventory(List<Item> inventoryList) throws IOException {
        FileWriter fw = new FileWriter("src/main/resources/inventory.txt");
        fw.write("");
        fw.close();

        FileWriter saveInventoryWriter = new FileWriter("src/main/resources/inventory.txt", true);

        for (Item item : inventoryList) {
            String textLine = String.format("%s,%s,%s,%.2f,%s\n", item.getItemCategory(), item.getItemId(), item.getItemName(), item.getItemPrice(), item.getItemQuantity());
            saveInventoryWriter.write(textLine);
        }
        saveInventoryWriter.close();

    }

    protected void deleteItemFromInventory(int idToDelete, List<Item> inventoryList, boolean updatingCategory) {
        for (Item item : inventoryList) {
            if (item.getItemId() == idToDelete) {
                inventoryList.remove(item);
                if (!updatingCategory) System.out.println("> Successfully deleted Item: " + item);
                return;
            }
        }

        System.out.printf("> Item # %s not found.\n", idToDelete);

    }

    protected void printCurrentInventory(List<Item> currentInventory) {
        String tableFormat = "| %-15s | %-8s | %-15s | %-8s | %-8s |\n";
        String edgeLine = "+-----------------+----------+-----------------+----------+----------+";
        String middleLine = "+=================+==========+=================+==========+==========+";
        System.out.println();
        System.out.println(edgeLine);
        System.out.format(tableFormat, "Category", "ID #", "Name", "Price", "Qty");
        System.out.println(middleLine);
        for (Item item : currentInventory) {
            System.out.format(tableFormat, item.getItemCategory(), item.getItemId(), item.getItemName(), String.format("$ %.2f", item.getItemPrice()), item.getItemQuantity());
        }
        System.out.println(edgeLine);

    }

}
